import React, { useEffect, useState, ChangeEvent, FormEvent } from 'react'
import './styles.css'
import { Link, useHistory } from 'react-router-dom'
import { FiArrowLeft } from 'react-icons/fi'
import api from '../../services/api'


const CreatePoint = () => {

    const [initialPosition, setInitialPosition] = useState<[number, number]>([0, 0])

    const [formData, setFormData] = useState({

        description: '',
        title: '',

    })


    const [selectedPosition, setSelectedPosition] = useState<[number, number]>([0, 0])
    const [selectedItems, setSelectedItems] = useState<number[]>([])
    const history = useHistory()

     function handleInputChange(event: ChangeEvent<HTMLInputElement>) {
        const { name, value } = event.target

        setFormData({ ...formData, [name]: value })
    }

    async function handleSubmit(event: FormEvent) {
        event.preventDefault()

        const { title, description} = formData;


        const data = {
            title,
            description
        }

       
        await api.post("/repositories", data)
            .then(response => {
                console.log(response);
            })
            .catch(error => {
                console.log(error);
            });
        history.push('/')
        
        alert("Projeto Criado")

    }

    

    return (
        <div id="page-create-point">
            <header>
                
                <Link to="/">
                    <FiArrowLeft />
                    Voltar pra home
                </Link>
            </header>

            <form onSubmit={handleSubmit}>
                <h1> Cadastro de Projetos</h1>

                <fieldset>
                    <legend>
                        <h2>Dados</h2>
                    </legend>

                    <div className="field">
                        <label htmlFor="title">titulo</label>

                        <input
                            type="text"
                            name="title"
                            id="title"
                            onChange={handleInputChange}
                            required
                        />
                    </div>

                    <div className="field">
                        <label htmlFor="description">description </label>

                        <input
                            type="text"
                            name="description"
                            id="description"
                            onChange={handleInputChange}
                            required
                        />
                    </div>


                </fieldset>

                <button type="submit">
                    Cadastrar Projeto
                </button>

            </form>

        </div>
    )

}

export default CreatePoint;