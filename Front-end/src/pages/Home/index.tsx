import React from 'react'
import {FiLogIn} from 'react-icons/fi'
import { Link } from 'react-router-dom'
import './styles.css'

const Home = () => {
    return (
        <div id="page-home">
            <div className="content">
                <header>

                </header>

                <main>
                    <h1>Projeto ELGIN - 2021</h1>
                    <p></p>

                    <Link to="/create-point">
                        <span>
                            <FiLogIn>

                            </FiLogIn>
                        </span>
                        <strong>Cadastrar Novo Projeto</strong>
                    </Link>
                </main>
            </div>
        </div>
    )

}

export default Home;