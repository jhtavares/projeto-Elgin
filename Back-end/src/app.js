const express = require("express")
const { uuid } = require("uuidv4")
const cors = require("cors")
const request = require("supertest");

const app = express()
app.use(express.json())
app.use(cors())

const repositories = []

app.get('/repositories', (request, response) => {
    return response.json(repositories)
})

app.post('/repositories',(request, response)=>{
    const body = request.body
    const {description, title} = request.body
    //const {description, title, techs} = request.body
    const repository = {id: uuid(), description, title}

    repositories.push(repository)
    return response.json(repository)
})

app.put('/repositories/:id', (request, response) =>{
    const { id } = request.params;
    const { title, description} = request.body;
    //const { title, description, techs} = request.body;
    const repositoryIndex = repositories.findIndex(repository => repository.id == id);

    if(repositoryIndex < 0){
        return response.status(400).json({erro: "Repository Not Found"})

    }
    const repository = {
        id, 
        title,
        description,
    }
    repositories[repositoryIndex] = repository
    return response.json(repository)
})

app.delete('/repositories/:id', (request, response) => {
    const { id } = request.params;
    const repositoryIndex = repositories.findIndex(repository => repository.id == id)

    if(repositoryIndex < 0){
        return response.status(400).json({error : "Repository Not Found"})
    }
    repositories.splice(repositoryIndex, 1)
    return response.status(204).json(repositories)
})

module.exports = app
