<h1> Projeto Engie 2021 </h1>
<p> Foi feito um sistema de cadastro de projetos, os projetos podem ser cadastrados através de uma página web,
e sua consulta assim como sua excluisão podem ser feitas através de um aplicativo mobile
</p>

<h3>Tecnologias utilizadas</h3>

- [ ] react
- [ ] react-native
- [ ] node.js
- [ ] docker
- [ ] docker-compose
- [ ] gitlab-ci/cd



<p>Infelizmente não foi possível ser finalizada a parte de CI/CD</p>

<p>Para iniciar a aplicação é necessário executar "yarn install" nos diretórios Front-end, Back-end a parte Mobile terá um mais de trabalho tendo que o pc tenha configurações do react-native (npx), caso tenha, inicia-se o emulador android, e no diretório raiz executa-se "npx react-native start" e "npx react-native run-android", <b>apenas com o app mobile é possível ver a lista de projetos cadastrados e excluí-los.</b>


