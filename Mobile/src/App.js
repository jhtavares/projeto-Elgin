import React, {useState, useEffect} from "react";
import api from './services/api'
import {
  SafeAreaView,
  View,
  FlatList,
  Text,
  StatusBar,
  StyleSheet,
  TouchableOpacity,
} from "react-native";

export default function App() {

  async function deleteDeveloper(id) {
    // Implement "Like Repository" functionality
   //
    const response = await api.delete(`repositories/${id}`) // retorna 

    const likedRepository = response.data

    const repositoriesUpdated = repositories.map(repository => {
 
    })
    setRepositories(repositories.filter(repository => repository.id !== id))
  }

  const [repositories, setRepositories] = useState([])

    api.get('/repositories').then(response =>{
          setRepositories(response.data)

    })
    .catch(function (error) {
      console.log(error);
    });

  return (
    <>
      <StatusBar barStyle="light-content" backgroundColor="#7159c1" />

      <SafeAreaView style={styles.container}>

        
        <FlatList
          data = {repositories}
          keyExtractor = {repository => repository.id}
          renderItem={({item: repository}) => (

            <View style={styles.repositoryContainer}>
              <Text style={styles.repository}>{repository.title}</Text>

              <View style={styles.techsContainer}>
                <Text style={styles.tech}>{repository.description}</Text>

              </View>

              <TouchableOpacity
                style={styles.button}
                onPress={() => deleteDeveloper(repository.id)}
              >
                <Text style={styles.buttonText}>EXCLUIR</Text>
            </TouchableOpacity>

          </View>
          )}
        />

      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#7159c1",
    
  },
  repositoryContainer: {
    marginBottom: 15,
    marginHorizontal: 15,
    backgroundColor: "#fff",
    padding: 20,
  },
  repository: {
    fontSize: 25,
    fontWeight: "bold",
    backgroundColor: "#04d361",
    padding: 20,
    borderRadius: 10


  },
  techsContainer: {
    flexDirection: "row",
    marginTop: 10,
  },
  tech: {
    fontSize: 20,
    fontWeight: "bold",
    marginRight: 10,
    paddingHorizontal: 20,
    paddingVertical: 20,
    backgroundColor: "#E0FFFF",
    borderRadius: 10

  },
 
  buttonText: {
    fontSize: 14,
    fontWeight: "bold",
    marginRight: 20,
    marginLeft: 20,
    marginTop: 20,
    borderRadius: 20,
    color: "#fff",
    backgroundColor: "#7159c1",
    padding: 15,
    textAlign: "center",
  },
});
